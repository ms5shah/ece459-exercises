use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();


    for i in 0..N {
        let tx1 = mpsc::Sender::clone(&tx);
        thread::spawn(move || {
            tx1.send(format!("hello from thread {}",i)).unwrap();
            println!("thread {} is finished",i);
            thread::sleep(Duration::from_secs(1));
    });

    }


    
    for received in rx {
        println!("Got: {}", received);
    }
}
